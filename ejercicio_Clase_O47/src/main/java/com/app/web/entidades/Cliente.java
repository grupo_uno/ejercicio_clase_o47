package com.app.web.entidades;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="clientes")
public class Cliente {
	//ATRIBUTOS 
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idCliente; 
	@Column(name="documentoCliente", nullable= false, length=15)
	private String documentoCliente; 
	@Column(name="tipoDocCliente", nullable= false, length=4)
	private String tipoDocCliente; 
	@Column(name="nombreCliente", nullable= false, length=30)
	private String nombreCliente; 
	@Column(name="apellidoCliente", nullable= false, length=30)
	private String apellidoCliente; 
	@Column(name="emailCliente", nullable= false, length=50, unique=true)
	private String emailCliente;
	
	//LLAVE FORANEA 
	@OneToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY, optional=false)
	@JoinColumn(name="idDireccionFK", referencedColumnName="idDireccion")
	private Direccion direccionCliente; 
	
	//LLAVE FORANEA CON VEHICULO
	@OneToMany(mappedBy="vehiculoCliente")
	private Set<Vehiculo> vehiculos; 
	
	//CONSTRUCTORES 
	public Cliente() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Cliente(int idCliente, String documentoCliente, String tipoDocCliente, String nombreCliente,
			String apellidoCliente, String emailCliente, Direccion direccionCliente, Set<Vehiculo> vehiculos) {
		super();
		this.idCliente = idCliente;
		this.documentoCliente = documentoCliente;
		this.tipoDocCliente = tipoDocCliente;
		this.nombreCliente = nombreCliente;
		this.apellidoCliente = apellidoCliente;
		this.emailCliente = emailCliente;
		this.direccionCliente = direccionCliente;
		this.vehiculos = vehiculos;
	}





	//GETTER AND SETTER - ENCAPSULAR 
	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public String getDocumentoCliente() {
		return documentoCliente;
	}

	public void setDocumentoCliente(String documentoCliente) {
		this.documentoCliente = documentoCliente;
	}

	public String getTipoDocCliente() {
		return tipoDocCliente;
	}

	public void setTipoDocCliente(String tipoDocCliente) {
		this.tipoDocCliente = tipoDocCliente;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getApellidoCliente() {
		return apellidoCliente;
	}

	public void setApellidoCliente(String apellidoCliente) {
		this.apellidoCliente = apellidoCliente;
	}

	public String getEmailCliente() {
		return emailCliente;
	}

	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}
	
	
	public Direccion getDireccionCliente() {
		return direccionCliente;
	}



	public void setDireccionCliente(Direccion direccionCliente) {
		this.direccionCliente = direccionCliente;
	}
	

	public Set<Vehiculo> getVehiculos() {
		return vehiculos;
	}


	public void setVehiculos(Set<Vehiculo> vehiculos) {
		this.vehiculos = vehiculos;
	}
	
	//SOBRE ESCRIBIR EL METODO TO STRING 
	@Override
	public String toString() {
		return "Cliente [idCliente=" + idCliente + ", documentoCliente=" + documentoCliente + ", tipoDocCliente="
				+ tipoDocCliente + ", nombreCliente=" + nombreCliente + ", apellidoCliente=" + apellidoCliente
				+ ", emailCliente=" + emailCliente + ", direccionCliente=" + direccionCliente + ", vehiculos="
				+ vehiculos + "]";
	}


	
	
}
