package com.app.web.entidades;

import javax.persistence.*;

@Entity
@Table(name="vehiculos")
public class Vehiculo {
	//ATRIBUTOS 
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int IdVehiculo; 
	@Column(name="placaVehiculo", nullable=false, length=6, unique=true)
	private String placaVehiculo; 
	@Column(name="marcaVehiculo", nullable=true, length=30)
	private String marcaVehiculo;
	@Column(name="tipoVehiculo", nullable=false, length=30)
	private String tipoVehiculo; 
	
	//CLAVE FORANEA DE CLIENTE
	@ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="idClienteFK", referencedColumnName="idCliente")
	private Cliente vehiculoCliente;

	//CONSTRUCTOR 
	public Vehiculo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Vehiculo(int idVehiculo, String placaVehiculo, String marcaVehiculo, String tipoVehiculo,
			Cliente vehiculoCliente) {
		super();
		IdVehiculo = idVehiculo;
		this.placaVehiculo = placaVehiculo;
		this.marcaVehiculo = marcaVehiculo;
		this.tipoVehiculo = tipoVehiculo;
		this.vehiculoCliente = vehiculoCliente;
	}

	//METODOS GETTER AND SETTER 
	public int getIdVehiculo() {
		return IdVehiculo;
	}

	public void setIdVehiculo(int idVehiculo) {
		IdVehiculo = idVehiculo;
	}

	public String getPlacaVehiculo() {
		return placaVehiculo;
	}

	public void setPlacaVehiculo(String placaVehiculo) {
		this.placaVehiculo = placaVehiculo;
	}

	public String getMarcaVehiculo() {
		return marcaVehiculo;
	}

	public void setMarcaVehiculo(String marcaVehiculo) {
		this.marcaVehiculo = marcaVehiculo;
	}

	public String getTipoVehiculo() {
		return tipoVehiculo;
	}

	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

	public Cliente getVehiculoCliente() {
		return vehiculoCliente;
	}

	public void setVehiculoCliente(Cliente vehiculoCliente) {
		this.vehiculoCliente = vehiculoCliente;
	}

	//METODO TO STRING 
	@Override
	public String toString() {
		return "Vehiculo [IdVehiculo=" + IdVehiculo + ", placaVehiculo=" + placaVehiculo + ", marcaVehiculo="
				+ marcaVehiculo + ", tipoVehiculo=" + tipoVehiculo + ", vehiculoCliente=" + vehiculoCliente + "]";
	} 
	

}
