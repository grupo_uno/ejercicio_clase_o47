package com.app.web.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.app.web.servicios.ClienteServicio;

@Controller
public class ClienteControl {
	
	//ATRIBUTO
	@Autowired
	private ClienteServicio servicio; 
	
	//METODO PARA LA PETICION DE LISTAR
	@GetMapping({"/","/cliente/lista"})
	public String listarClientes(Model modelo) {
		modelo.addAttribute("clientes", servicio.listarClientes()); 
		return "cliente/lista"; 
	}
	
	
}
