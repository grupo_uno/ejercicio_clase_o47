package com.app.web.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.web.entidades.Cliente;
import com.app.web.repositorios.ClienteRepositorio;

@Service
public class ClienteServicioImp 
	implements ClienteServicio{

	@Autowired
	private ClienteRepositorio repositorio; 
	
	//MICROSERVICIO PARA LISTAR  
	@Override
	public List<Cliente> listarClientes() {
		return repositorio.findAll();
	}
	
	

}
